'use strict';
const db = require('../models');
module.exports = {
  async up(queryInterface, Sequelize) {

    const saltA = await db.users.generateSalt();
    const saltB = await db.users.generateSalt();
    const passwordA = await db.users.hashPassword("Alpha@123456", saltA);
    const passwordB = await db.users.hashPassword("Beta@123456", saltB);
    // console.log(`salt= ${saltA} password=${passwordB}`);
    const seedData = [{
      fullname:'Alpha',
      email:'alpha@mailinator.com',
      password:passwordA,
      salt:saltA,
    },{
      fullname:'Beta',
      email:'beta@mailinator.com',
      password:passwordB,
      salt:saltB,
    }];
    await db.users.bulkCreate(seedData);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
