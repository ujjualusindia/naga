var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Ujjual\'s Machine test.' });
});

router.use('/api', require('../app/api/'));

module.exports = router;
