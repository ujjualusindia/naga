const express = require('express')

const router = express.Router();
const controller = require('./controller');
const validationMiddleware = require('../../../middleware/validation');
router.post('/login',validationMiddleware.login, controller.login);

module.exports = router
