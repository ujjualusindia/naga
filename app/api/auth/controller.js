const db = require('../../../models');

exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
        if (!email)
            throw new Error('Invalid email');
        if (!password)
            throw new Error('Invalid email');

        const thisUser = await db.users.findOne({
            where: {
                email
            },
            raw: true
        });
        if (!thisUser) throw new Error('user not found');

        const validPassword = await db.users.verifyPassword(password, thisUser.password, thisUser.salt)

        if (!validPassword) throw new Error('incorrect password');

        const accessToken = db.users.generateAuthToken(thisUser);


        return res.status(200)
            .json({
                success: true,
                message: "login successful",
                data: {
                    ...thisUser,
                    accessToken
                }
            });

    } catch (error) {
        return res
            .status(412)
            .json({
                success: false,
                message: error.message,
                data: []
            });
    }
}