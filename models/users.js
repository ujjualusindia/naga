'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {
  jwt: { secretkey, expires },
} = require('../config');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  users.init({
    fullname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    salt: DataTypes.STRING

  }, {
    sequelize,
    modelName: 'users',
  });

  // token #start

  users.validatePassword = function (pass) {
    return /^(?=.*\d).{8,}$/.test(pass)
  };

  users.generateSalt = async function () {
    return await bcrypt.genSalt()
  };

  users.hashPassword = async function (pass, salt) {
    return await bcrypt.hash(pass, salt)
  };

  users.verifyPassword = async function (pass, hash, salt) {
    const hashPassword = await bcrypt.hash(pass, salt)
    if (hashPassword == hash) return true;
    else return false;
  };

  users.generateAuthToken = function (data) {
    let expiresIn = expireIn(10); //

    if (data.rememberMe) {
      console.log("entered----");
      expiresIn = expireIn(720)
    }

    return jwt.sign({
      id: data.id,
      email: data.email,
      validity: data.password.concat(data.id).concat(data.email)

    },
      secretkey, { expiresIn }
    )
  };

  // token #end
  return users;
};

const expireIn = numDays => {
  const dateObj = new Date();
  return dateObj.setMinutes(dateObj.getMinutes() + numDays);
};