const validator = require('../helper/validator');

const login = (req, res, next) => {
    const validationRule = {
        "email": "required|email",
        "password": "required|string|min:6",
    }
    validator(req.body, validationRule, {}, (err, status) => {
        if (!status) {
            res.status(412)
                .send({
                    success: false,
                    message: 'Validation failed',
                    data: err
                });
        } else {
            next();
        }
    });
}

module.exports = { 
  login
}